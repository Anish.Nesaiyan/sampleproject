package week4.day2;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import week4.day2.SeMethods;

public class ProjectMethods extends SeMethods {

	@Parameters({"url", "userID", "pWd"})
	@BeforeMethod
	public void loginleaftaps(String link, String id, String pwd) {
		startApp("chrome", link);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, id);
		WebElement elePassword = locateElement("password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("linktext", "CRM/SFA");
		click(crm);

	}

	@AfterMethod
	public void close1() {

		closeBrowser();
	}
	
	public int tommorrow()
	{
		
		Date date= new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		String today= sdf.format(date);
		int tommorrow =Integer.parseInt(today)+1;
		return tommorrow;
		//String tmr = tommorrow;
		//System.out.println(tommorrow);
		//System.out.println(today);
		
	}
}





