package testCases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week4.day2.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods {
	@Test
	public void mergeLead() {
		WebElement mergelead = locateElement("linktext", "Merge Leads");
		click(mergelead);
	}

}
