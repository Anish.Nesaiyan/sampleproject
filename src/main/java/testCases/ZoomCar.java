package testCases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week4.day2.ProjectMethods;

public class ZoomCar extends ProjectMethods {

	@Test
	public void zoomCar() throws InterruptedException {

		startApp("chrome", "https://www.zoomcar.com");
		WebElement chennai = locateElement("xpath", "//img[@alt='Chennai']");
		click(chennai);
		WebElement start = locateElement("linktext", "Start your wonderful journey");
		click(start);
		WebElement search = locateElement("xpath", "//div[@class='component-popular-locations']/div[2]");
		click(search);
		WebElement locate = locateElement("xpath","//img[@src='/assets/component-images/location.4f83fc7a3f686c9a14dc757b7a0b9b07.png']");
		click(locate);
		WebElement next1 = locateElement("class", "proceed");
		click(next1);
		int tmrw = tommorrow();
		WebElement clickdate = locateElement("xpath", "//div[contains(text(),'" + tmrw + "')]");
		click(clickdate);
		clickdate.isSelected();
		click(next1);
		click(next1);
		Thread.sleep(2000);
		List<WebElement> xp = driver.findElementsByXPath("//div[@class='price']");
		System.out.println("Total number of listed values" + xp.size());
		List<Integer> ls = new ArrayList<>();
		for (int i = 1; i <= xp.size(); i++) {
			String val = locateElement("xpath", "(//div[@class='price'])[" + i + "]").getText();
			System.out.println(val);
			int length = val.length();
			String val2 = val.substring(2, length);
			System.out.println(val2);
			int result = Integer.parseInt(val2);
			ls.add(result);
		}
		Integer max = Collections.max(ls);
		int indexOf = ls.indexOf(max);
		indexOf=indexOf+1;
		System.out.println("Max car price is:"+max);
		System.out.println(indexOf);
		String text = locateElement("xpath", "(//div[@class='details'])[" + indexOf + "]/h3").getText();
		System.out.println("The largest price car is: "+text);
		WebElement large = locateElement("xpath", "(//button[@name='book-now'])[" + indexOf + "]");
		click(large);
	}

}
