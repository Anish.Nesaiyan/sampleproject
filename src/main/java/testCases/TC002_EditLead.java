package testCases;


import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;

import week4.day2.*;

public class TC002_EditLead extends ProjectMethods {

	@Test(groups="sanity",dependsOnGroups="smoke")
			//dependsOnMethods="testCases.TC001_CreateLead.createLead")
			//invocationCount=2,invocationTimeOut=200000)
	public void editLead() throws InterruptedException {

		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		WebElement eletypecomname = locateElement("id", "createLeadForm_companyName");
		type(eletypecomname, "Intellect");
		WebElement eletypefrstname = locateElement("id", "createLeadForm_firstName");
		type(eletypefrstname, "Anish");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "Intellect");
		WebElement button = locateElement("name", "submitButton");
		click(button);
		WebElement editbutton = locateElement("linktext", "Edit");
		click(editbutton);
		WebElement eletypecomnameEdt = locateElement("id", "updateLeadForm_companyName");
		eletypecomnameEdt.clear();
		type(eletypecomnameEdt, "Fidelity");
		Thread.sleep(2000);
		WebElement update = locateElement("xpath", "//input[@value='Update']");
		click(update);
		WebElement afterupdate = locateElement("id", "viewLead_companyName_sp");
		String name = getText(afterupdate);
		if (name.contains("Fidelity")) {
			System.out.println("Update succesfull");
		} else {

			System.out.println("Not successful");
		}
		//closeAllBrowsers();
	}

}
