package testCases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.readExcel;
import week4.day2.*;

public class TC001_CreateLead4_ExcelDataProvider extends ProjectMethods {

	@Test(dataProvider="CreateLead")
			//enabled=true)
	public void createLead(String cName, String fName, String lName) {

		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		WebElement eletypecomname = locateElement("id", "createLeadForm_companyName");
		type(eletypecomname, cName);
		WebElement eletypefrstname = locateElement("id", "createLeadForm_firstName");
		type(eletypefrstname, fName);
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, lName);
		WebElement button = locateElement("name", "submitButton");
		click(button);

	}
	
	@DataProvider(name="CreateLead",indices= {1,0})
	public Object[][] fetch() throws IOException
	
	{
		Object[][] readEx = readExcel.readExcel1();
		return readEx;
	
		/*Object[][] data= new Object[2][3];
		data[0][0]="Intellect";
		data[0][1]="Anish";
		data[0][2]="N";
		
		data[1][0]="FID";
		data[1][1]="Anish";
		data[1][2]="Nesaiyan";
		
		return data;*/		
	}

}
