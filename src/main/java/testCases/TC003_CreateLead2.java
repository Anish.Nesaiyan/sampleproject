package testCases;

import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;

import week4.day2.*;

public class TC003_CreateLead2 extends ProjectMethods {

	@Test(groups="regression",dependsOnGroups="sanity")
			//enabled=true)
	public void createLead() {

		WebElement createlead = locateElement("linktext", "Create Lead");
		click(createlead);
		WebElement eletypecomname = locateElement("id", "createLeadForm_companyName");
		type(eletypecomname, "Intellect");
		WebElement eletypefrstname = locateElement("id", "createLeadForm_firstName");
		type(eletypefrstname, "Anish");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "Intellect");
		WebElement button = locateElement("name", "submitButton");
		click(button);

	}

}
