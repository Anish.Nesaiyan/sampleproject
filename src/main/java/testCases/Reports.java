package testCases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	@Test
	public void reports() throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./report/reports.html");
		html.setAppendExisting(true);// To append each report if true and overide other reports if false
		
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger = extent.createTest("Anish fever", "Fever check");
		logger.assignAuthor("Anish");
		logger.assignCategory("Smoke");
		
		logger.log(Status.PASS, "No fever", 
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
		
		logger.log(Status.PASS, "High Fever", 
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		
		logger.log(Status.PASS, "Fraud fever", 
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		
		extent.flush();

	}

}
