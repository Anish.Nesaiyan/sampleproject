package testCases;

import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;

import week4.day2.*;

public class TC001_CreateLead extends ProjectMethods {

	@Test(groups="smoke")
			//enabled=true)
	public void createLead() {

		WebElement createlead = locateElement("linktext", "Create Lead1");
		click(createlead);
		WebElement eletypecomname = locateElement("id", "createLeadForm_companyName");
		type(eletypecomname, "Intellect");
		WebElement eletypefrstname = locateElement("id", "createLeadForm_firstName");
		type(eletypefrstname, "Anish");
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, "Intellect");
		WebElement button = locateElement("name", "submitButton");
		click(button);

	}

}
