package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class readExcel {

	//@Test
	public static Object[][] readExcel1() throws IOException {
		// Create workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		// go to sheet
		XSSFSheet sheet = wb.getSheetAt(0);
		
		int columnCount = sheet.getRow(0).getLastCellNum();
		// row count
		int rowCount = sheet.getLastRowNum();
		
		Object[][] obj= new Object[rowCount][columnCount];

		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j <columnCount; j++) {

				XSSFCell cell = row.getCell(j);
				String value = cell.getStringCellValue();
				obj[i-1][j]= value;
				System.out.println(value);
				

			}

		}
		return obj;
	}

}
